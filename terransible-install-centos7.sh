#!/bin/bash

# Update then install Ansible, Terraform and deps
sudo yum update -y ; \
sudo yum install -y \
    epel-release \
    ansible \
    ansible-galaxy \
    wget \
    unzip \
    awscli \
    curl
    
sudo yum-config-manager --enable epel

# Install Terraform    
wget https://releases.hashicorp.com/terraform/0.12.17/terraform_0.12.17_linux_amd64.zip
unzip terraform_0.12.17_linux_amd64.zip
sudo cp terraform /usr/local/bin