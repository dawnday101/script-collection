#!/bin/bash

# Install Basics
sudo apt-get update -y 
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    tree \
    make \
    ansible

# Install Docker and Repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io
sudo usermod -aG docker $USER

cat << EOF > /etc/docker/daemon.json 
{
    "insecure-registries" : [ "172.30.0.0/16" ]
}
EOF
sudo service docker restart

cd /home/ubuntu
# Install Openshift
wget https://github.com/openshift/origin/releases/download/v3.9.0/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz
tar -zvxf openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz
cd openshift-origin-client-tools-v3.9.0-191fece-linux-64bit
sudo cp oc /usr/local/bin/

# Install kubernetes-cli
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

# Install GO
cd /tmp
wget https://dl.google.com/go/go1.13.5.linux-amd64.tar.gz
sudo tar xf go1.13.5.linux-amd64.tar.gz
sudo mv -f go /usr/local

# Append ~/profile
echo "export GOROOT=/usr/local/go
	export GOPATH=$HOME/go
	export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> ~/.profile
	
# Append ~/.bashrc
echo "export GOROOT=/usr/local/go
	export GOPATH=$HOME/go
	export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> ~/.bashrc

. ~/.bashrc
. ~/.profile
echo "##########################"
echo $PATH
echo "##############################"
cd /home/ubuntu

#Install Operator-SDK
#RELEASE_VERSION=v0.13.0
#curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu
#verify downloaded binary
#curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc
#gpg --verify operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu.asc
# replace $KEY_ID with output from previous command
#gpg --recv-key "$KEY_ID"
#gpg --keyserver keyserver.ubuntu.com --recv-key "$KEY_ID"
# Install binary in path
#chmod +x operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu && sudo mkdir -p /usr/local/bin/ && sudo cp operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu /usr/local/bin/operator-sdk && rm operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu

############## Alternate compile and install from masteer
go get -d github.com/operator-framework/operator-sdk # This will download the git repository and not install it
cd $GOPATH/src/github.com/operator-framework/operator-sdk
git checkout master
make tidy
make install
###############

# Install OLM
curl -L https://github.com/operator-framework/operator-lifecycle-manager/releases/download/0.13.0/install.sh -o install.sh
chmod +x install.sh

