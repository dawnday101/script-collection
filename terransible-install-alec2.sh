#!/bin/bash

# Update then install Ansible, Terraform and deps
sudo yum update -y ; \
sudo yum install -y \
#    epel-release \
#    ansible2 \
#    ansible-galaxy \
    wget \
    unzip \
    awscli \
    curl

sudo amazon-linux-extras install -y \
    epel \
    ansible2
    
sudo yum-config-manager --enable epel

# Install Terraform    
wget https://releases.hashicorp.com/terraform/0.12.17/terraform_0.12.17_linux_amd64.zip
unzip terraform_0.12.17_linux_amd64.zip
sudo cp terraform /usr/local/bin

# TO DO - figure out if possible how to pass variables into AWS configure prompt
#Add AWS variables
#AWS_ACCESS_KEY_ID="AKIAI5CAJTDVAEUHDZSQ"
#AWS_SECRET_ACCESS_KEY="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"
#AWS_DEFAULT_REGION=gov-east-1

#aws configure
