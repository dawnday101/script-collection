provider "vsphere" {
  user           = "${var.vsphere_user}"
  password       = "${var.vsphere_password}"
  vsphere_server = "${var.vsphere_server}"
  //vsphere_port   = "${var.vsphere_port}"

  # If you have a self-signed cert
  allow_unverified_ssl = true
}
