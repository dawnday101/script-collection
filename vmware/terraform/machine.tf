resource "vsphere_virtual_machine" "basebox" {
  name = "${var.basebox_name}"
  resource_pool_id = "${data.vsphere_compute_cluster.compute_cluster.resource_pool_id}"
  datastore_id = "${data.vsphere_datastore.datastore.id}"

  num_cpus = "${var.cpus}"
  memory = "${var.mem_num}"
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
  wait_for_guest_net_routable = false

  network_interface {
    network_id = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label = "disk0"
    size = "${var.basebox_disk_size}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
    unit_number = 0
  }
  disk {
    label = "disk1"
    size = "100"
    thin_provisioned = true
    unit_number = 1
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "${var.basebox_name}"
	      domain = "${var.netdomain}"
      }

      network_interface {
	      ipv4_address = "${var.basebox_ip}"
        ipv4_netmask = "${var.net_mask}"
      }

      timeouts {
    	  create = "60m"
    	  delete = "2h"
      }
    }
  }
}

resource "vsphere_virtual_machine" "node" {
  count = "${var.node_count_num}"
  name = "${var.vmname}-${count.index}"
  resource_pool_id = "${data.vsphere_compute_cluster.compute_cluster.resource_pool_id}"
  datastore_id = "${data.vsphere_datastore.datastore.id}"

  num_cpus = "${var.cpus}"
  memory = "${var.mem_num}"
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
  wait_for_guest_net_routable = false

  network_interface {
    network_id = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    label = "disk0"
    size = "${var.node_disk0_size}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
    unit_number = 0
  }

  disk {
    label = "disk1"
    size = "100"
    thin_provisioned = true
    unit_number = 1
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "${var.vmname}-${count.index}"
	      domain = "${var.netdomain}"
      }

      network_interface {
	      ipv4_address = "${var.node_ip_prefix}.${var.node_ip_start+count.index}"
        ipv4_netmask = "${var.net_mask}"
      }

      timeouts {
    	  create = "60m"
    	  delete = "2h"
      }
    }
  }
}
